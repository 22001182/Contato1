//
//  ViewController.swift
//  Contato
//
//  Created by COTEMIG on 01/09/22.
//

import UIKit

struct Contato {
    let nome: String
    let numero: String
    let endereco: String
    let email: String
}

class ViewController: UIViewController, UITableViewDataSource {
    
    var listaDeContatos:[Contato] = []
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return listaDeContatos.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableview.dequeueReusableCell(withIdentifier: "MinhaCelula", for: indexPath ) as! MyCell
        let Contatos = listaDeContatos[indexPath.row]
        
        cell.nome.text = Contatos.nome
        cell.numero.text = Contatos.numero
        cell.email.text = Contatos.email
        cell.endereco.text = Contatos.endereco
        
        return cell
    }
    

    @IBOutlet weak var tableview: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
     
        tableview.dataSource = self
        
        listaDeContatos.append(Contato(nome: "Contato1", numero: "31 9999999", endereco:" Rua dos contatos , 11", email: "contato1@email.com.br"))
        
        listaDeContatos.append(Contato(nome: "Contato2", numero: "31 9999-9999", endereco: "Rua dos contatos,12", email: "contato2@email.com.br"))
        
        
        listaDeContatos.append(Contato(nome: "Contato3 ", numero: "31 9999-9999", endereco: "Rua dos contatos,13", email: "contato3@email.com.br"))
        
        
        
       
    }


}

